package com.naufalnibros.kuliahmobile

import android.Manifest
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.naufalnibros.kuliahmobile.databinding.ActivityMainBinding
import com.naufalnibros.kuliahmobile.ui.learning_10.Learning10Activity
import com.naufalnibros.kuliahmobile.ui.learning_10.Learning10BActivity
import com.naufalnibros.kuliahmobile.ui.learning_10.Learning10CActivity
import com.naufalnibros.kuliahmobile.ui.learning_10.Learning10DActivity
import com.naufalnibros.kuliahmobile.ui.learning_11.Learning11Activity
import com.naufalnibros.kuliahmobile.ui.learning_12.Learning12Activity
import com.naufalnibros.kuliahmobile.ui.learning_9.Learning9Activity
import com.naufalnibros.kuliahmobile.utils.viewBinding

class MainActivity : AppCompatActivity(), MainAdapter.OnItemListener {

    private val binding by viewBinding(ActivityMainBinding::inflate)

    private val adapter = MainAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(
                    report: MultiplePermissionsReport)
                { /* ... */ }
                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) { /* ... */ }
            }).check()

        binding.recyclerview.adapter = adapter
        adapter.setListener(this)
        adapter.setList(listOf(
                "( Latihan 1 ) EKSPLORASI PENGAMBILAN KEPUTUSAN",
                "( Latihan 2 ) EKSPLORASI PENGAMBILAN KEPUTUSAN",
                "( Latihan 3 ) EKSPLORASI PENGAMBILAN KEPUTUSAN",
                "( Latihan 4 ) EKSPLORASI PENGAMBILAN KEPUTUSAN",
                "( Latihan 5 ) EKSPLORASI PENGAMBILAN KEPUTUSAN",
                "( Latihan 6 ) EKSPLORASI PENGAMBILAN KEPUTUSAN",
                "Learning 10 - ( Text To Speech )",
                "Learning 10 - ( Aplikasi Maps )",
                "Learning 10 - ( Aplikasi Input Data to Excel )",
                "Learning 10 - ( Aplikasi GPS )",
                "Learning 11 - ( Game Kuis )",
                "Learning 12 - ( Login Using Rest API )"
        ))
    }

    override fun onItemCLick(position: Int) {
        when(position) {
            0 -> Learning9Activity.start(this, 1)
            1 -> Learning9Activity.start(this, 2)
            2 -> Learning9Activity.start(this, 3)
            3 -> Learning9Activity.start(this, 4)
            4 -> Learning9Activity.start(this, 5)
            5 -> Learning9Activity.start(this, 6)
            6 -> Learning10Activity.start(this)
            7 -> Learning10BActivity.start(this)
            8 -> Learning10CActivity.start(this)
            9 -> Learning10DActivity.start(this)
            10 -> Learning11Activity.start(this)
            11 -> Learning12Activity.start(this)
        }
    }

}