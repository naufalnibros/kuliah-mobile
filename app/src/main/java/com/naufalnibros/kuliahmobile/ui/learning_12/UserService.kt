package com.naufalnibros.kuliahmobile.ui.learning_12

import io.reactivex.Observable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface UserService {

    @POST("user/login")
    @FormUrlEncoded
    fun login(
        @Field("nip") nip: String, 
        @Field("password") pass: String
    ): Observable<User>

}