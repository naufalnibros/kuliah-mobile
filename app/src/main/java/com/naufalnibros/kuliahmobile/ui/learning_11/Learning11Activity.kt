package com.naufalnibros.kuliahmobile.ui.learning_11

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.naufalnibros.kuliahmobile.databinding.ActivityLearning11Binding
import com.naufalnibros.kuliahmobile.utils.viewBinding

class Learning11Activity : AppCompatActivity() {

    private val binding by viewBinding(ActivityLearning11Binding::inflate)

    private val list = listOf(
        Quiz("Sintaks java untuk melakukan kompilasi terhadap berkas program adalah",
            "A. java",
            "B. javac",
            "C. javaclass",
            "D. javax",
            "B. javac"),
        Quiz("Diantara perintah untuk mencetak berikut, yang benar adalah",
            "A. System.out.println(“Hello world”);",
            "B. System.Out.println(“Hello world ”);",
            "C. System.out.Println(“Hello world ”);",
            "D. System.Out.Println(Hello world ”);",
            "A. System.out.println(“Hello world”);"),
        Quiz("System yang berguna untuk mengirim keluaran ke layar adalah",
            "A. System.in",
            "B. System.out",
            "C. System.err",
            "D. System.exit",
            "B. System.out"),
        Quiz("Hasil kompilasi dari berkas java adalah",
            "A. File BAK",
            "B. File Bytecode",
            "C. File executable",
            "D. File class",
            "B. File Bytecode"),
        Quiz("Berikut ini yang termasuk tipe data primitive adalah",
            "A. Boolean",
            "B. character",
            "C. byte",
            "D. Double",
            "C. byte")
    )

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, Learning11Activity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        var nomor = 0
        var benar = 0
        setSoal(nomor)

        binding.proses.setOnClickListener {
            if ((list.size - 1) >= nomor) {
                Toast.makeText(this, "jawaban benar : $benar", Toast.LENGTH_LONG).show()
            } else {
                if (findViewById<RadioButton>(binding.grupPilihanGanda.checkedRadioButtonId).text == list[nomor].jawaban) {
                    benar++
                }
                nomor++
                setSoal(nomor)
            }
        }

    }

    private fun setSoal(nomor: Int) {
        binding.soal.text = list[nomor].soal
        binding.pilihanA.text = list[nomor].a
        binding.pilihanB.text = list[nomor].b
        binding.pilihanC.text = list[nomor].c
        binding.pilihanD.text = list[nomor].d
    }

    data class Quiz(
        val soal: String,
        val a: String,
        val b: String,
        val c: String,
        val d: String,
        val jawaban: String
    )
}