package com.naufalnibros.kuliahmobile.ui.learning_10

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.naufalnibros.kuliahmobile.R
import com.naufalnibros.kuliahmobile.databinding.ActivityLearning10BBinding
import com.naufalnibros.kuliahmobile.utils.viewBinding


class Learning10BActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityLearning10BBinding::inflate)

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, Learning10BActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_learning10_b)

        binding.proses.setOnClickListener {
            val gmmIntentUri: Uri = Uri.parse("geo:0,0?q=${binding.edt.text}")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }

    }
}