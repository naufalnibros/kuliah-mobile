package com.naufalnibros.kuliahmobile.ui.learning_12;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User extends ResponseModel implements Parcelable {

	@SerializedName("token")
	@Expose
	private String token;

	@SerializedName("id")
	@Expose
	private String id;

	@SerializedName("nama_lengkap")
	@Expose
	private String name;

	@SerializedName("nip")
	@Expose
	private String nip;

	@SerializedName("eselon")
	@Expose
	private String eselon;

	@SerializedName("pangkat")
	@Expose
	private String pangkat;

	@SerializedName("golongan")
	@Expose
	private String golongan;

	@SerializedName("gelar_depan")
	@Expose
	private String gelarDepan;

	@SerializedName("gelar_belakang")
	@Expose
	private String gelarBelakang;

	@SerializedName("satuan_kerja_anak")
	@Expose
	private String namaOPD;

	public User() {
	}

	protected User(Parcel in) {
		token = in.readString();
		id = in.readString();
		name = in.readString();
		nip = in.readString();
		eselon = in.readString();
		pangkat = in.readString();
		golongan = in.readString();
		gelarDepan = in.readString();
		gelarBelakang = in.readString();
		namaOPD = in.readString();
	}

	public static final Creator<User> CREATOR = new Creator<User>() {
		@Override
		public User createFromParcel(Parcel in) {
			return new User(in);
		}

		@Override
		public User[] newArray(int size) {
			return new User[size];
		}
	};

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getEselon() {
		return eselon;
	}

	public void setEselon(String eselon) {
		this.eselon = eselon;
	}

	public String getPangkat() {
		return pangkat;
	}

	public void setPangkat(String pangkat) {
		this.pangkat = pangkat;
	}

	public String getGolongan() {
		return golongan;
	}

	public void setGolongan(String golongan) {
		this.golongan = golongan;
	}

	public String getGelarDepan() {
		return gelarDepan;
	}

	public void setGelarDepan(String gelarDepan) {
		this.gelarDepan = gelarDepan;
	}

	public String getGelarBelakang() {
		return gelarBelakang;
	}

	public void setGelarBelakang(String gelarBelakang) {
		this.gelarBelakang = gelarBelakang;
	}

	public String getNamaOPD() {
		return namaOPD;
	}

	public void setNamaOPD(String namaOPD) {
		this.namaOPD = namaOPD;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(token);
		parcel.writeString(id);
		parcel.writeString(name);
		parcel.writeString(nip);
		parcel.writeString(eselon);
		parcel.writeString(pangkat);
		parcel.writeString(golongan);
		parcel.writeString(gelarDepan);
		parcel.writeString(gelarBelakang);
		parcel.writeString(namaOPD);
	}
}