package com.naufalnibros.kuliahmobile.ui.learning_10

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.naufalnibros.kuliahmobile.BuildConfig
import com.naufalnibros.kuliahmobile.databinding.ActivityLearning10DBinding
import com.naufalnibros.kuliahmobile.utils.viewBinding
import org.osmdroid.config.Configuration
import org.osmdroid.views.overlay.ScaleBarOverlay
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay

class Learning10DActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityLearning10DBinding::inflate)

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, Learning10DActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        Configuration.getInstance().userAgentValue = BuildConfig.APPLICATION_ID

        binding.mapview.controller.setZoom(17.15)
        binding.mapview.setMultiTouchControls(true)
        binding.mapview.isHorizontalMapRepetitionEnabled = false
        binding.mapview.isVerticalMapRepetitionEnabled = false

        binding.mapview.overlays.add(ScaleBarOverlay(binding.mapview))

        val myLocationNewOverlay = MyLocationNewOverlay(GpsMyLocationProvider(this), binding.mapview)
        myLocationNewOverlay.enableMyLocation()
        myLocationNewOverlay.enableFollowLocation()
        myLocationNewOverlay.isDrawAccuracyEnabled = true
        binding.mapview.overlays.add(myLocationNewOverlay)
    }
}