package com.naufalnibros.kuliahmobile.ui.learning_10

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.naufalnibros.kuliahmobile.databinding.ActivityLearning10Binding
import com.naufalnibros.kuliahmobile.utils.viewBinding
import java.util.*

class Learning10Activity : AppCompatActivity() {

    private var tts: TextToSpeech? = null

    private val binding by viewBinding(ActivityLearning10Binding::inflate)

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, Learning10Activity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        tts = TextToSpeech(
            applicationContext
        ) { status ->
            if (status != TextToSpeech.ERROR) {
                tts?.language = Locale.getDefault()
            }
        }

        binding.proses.setOnClickListener {
            val text = binding.edt.text.toString()
            Toast.makeText(this, "Text to Speech $text", Toast.LENGTH_LONG).show()
            tts?.speak(text, TextToSpeech.QUEUE_FLUSH, null)
        }
    }

    override fun onPause() {
        tts?.let {
            it.stop()
            it.shutdown()
        }
        super.onPause()
    }
}