package com.naufalnibros.kuliahmobile.ui.learning_10

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.naufalnibros.kuliahmobile.databinding.ActivityLearning10CBinding
import com.naufalnibros.kuliahmobile.ui.learning_9.Learning9Activity
import com.naufalnibros.kuliahmobile.utils.viewBinding
import java.io.File
import java.io.FileWriter


class Learning10CActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityLearning10CBinding::inflate)

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, Learning9Activity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val folder = File(filesDir, "kuliah-mobile.csv")

        object : Thread() {
            override fun run() {
                try {
                    val fw = FileWriter(folder)
                    fw.append("Nik")
                    fw.append(',')
                    fw.append("Nama")
                    fw.append(',')
                    fw.append("Alamat")
                    fw.append(',')
                    fw.append("tanggal")
                    fw.append('\n')
                    fw.append(binding.edtNik.text.toString())
                    fw.append(',')
                    fw.append(binding.edtNama.text.toString())
                    fw.append(',')
                    fw.append(binding.edtAlamat.text.toString())
                    fw.append(',')
                    fw.append(System.currentTimeMillis().toString())
                    fw.append('\n')
                    // fw.flush();
                    fw.close()

                    val csvIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        Intent(Intent.ACTION_OPEN_DOCUMENT)
                    } else {
                        Intent(Intent.ACTION_VIEW)
                    }
                    csvIntent.addCategory(Intent.CATEGORY_OPENABLE)
                    csvIntent.setDataAndType(FileProvider.getUriForFile(this@Learning10CActivity, "$packageName.provider", folder), "text/csv")
                    csvIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                    csvIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(csvIntent)

                } catch (e: Exception) {
                }
            }
        }.start()
    }
}