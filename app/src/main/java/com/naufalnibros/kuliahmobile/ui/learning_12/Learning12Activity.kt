package com.naufalnibros.kuliahmobile.ui.learning_12

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.naufalnibros.kuliahmobile.databinding.ActivityLearning12Binding
import com.naufalnibros.kuliahmobile.utils.viewBinding
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class Learning12Activity : AppCompatActivity() {

    private val binding by viewBinding(ActivityLearning12Binding::inflate)

    private var progressDialog: ProgressDialog? = null

    private val disposable = CompositeDisposable()

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, Learning12Activity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        progressDialog = ProgressDialog(this)
        progressDialog?.setCancelable(false)
        progressDialog?.setMessage("Proses authentikasi")

        binding.proses.setOnClickListener {
            progressDialog?.show()
            NetworkModule.provideService(NetworkModule.provideRetrofit(
                NetworkModule.provideHttpClient()
            ))
                .login(binding.edtUsername.text.toString(), binding.edtPassword.text.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnTerminate { progressDialog?.dismiss() }
                .subscribe(
                    {
                        progressDialog?.dismiss()
                    }, {
                        Toast.makeText(this, "Terjadi kesalahan : ${it.message}", Toast.LENGTH_LONG).show()
                    }, {
                        Toast.makeText(this, "Selamat Login berhasil", Toast.LENGTH_LONG).show()
                    }, {
                        disposable.add(it)
                    }
                )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }
}