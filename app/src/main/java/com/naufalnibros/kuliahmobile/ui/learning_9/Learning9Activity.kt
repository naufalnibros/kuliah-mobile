package com.naufalnibros.kuliahmobile.ui.learning_9

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.naufalnibros.kuliahmobile.databinding.ActivityLearning9Binding
import com.naufalnibros.kuliahmobile.utils.viewBinding
import java.util.*

class Learning9Activity : AppCompatActivity() {

    private val binding by viewBinding(ActivityLearning9Binding::inflate)

    companion object {
        fun start(context: Context, latihan: Int) {
            val intent = Intent(context, Learning9Activity::class.java)
            intent.putExtra("latihan", latihan)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        when(intent.getIntExtra("latihan", 0)) {
            1 -> {
                binding.form2.visibility = View.GONE
                binding.label.text = "Nilai 1 - 100"
                binding.input.inputType = InputType.TYPE_CLASS_NUMBER
                binding.proses.setOnClickListener {
                    binding.hasil.text =
                        if (binding.input.text.toString().toInt() <= 100)
                            "Nilai Valid"
                        else
                            "Nilai tidak Valid"
                }
            }
            2 -> {
                binding.form2.visibility = View.GONE
                binding.label.text = "Masukkan Huruf Kapital"
                binding.proses.setOnClickListener {
                    if (binding.input.length() != 0) {
                        binding.hasil.text =
                            if (binding.input.text[0].isUpperCase())
                                "Huruf Pertama Kapital"
                            else
                                "Huruf Pertama Bukan Kapital"
                    } else {
                        Toast.makeText(this, "Inputan tidak boleh kosong", Toast.LENGTH_LONG).show()
                    }
                }
            }
            3 -> {
                binding.form2.visibility = View.GONE
                binding.label.text = "Nilai 0 - 100"
                binding.input.inputType = InputType.TYPE_CLASS_NUMBER
                binding.proses.setOnClickListener {
                    val inputan = binding.input.text.toString().toInt()
                    binding.hasil.text =
                        when {
                            inputan < 50 -> {
                                "Skor E"
                            }
                            inputan in 50..59 -> {
                                "Skor D"
                            }
                            inputan in 60..69 -> {
                                "Skor C"
                            }
                            inputan in 70..89 -> {
                                "Skor B"
                            }
                            inputan in 90..100 -> {
                                "Skor A"
                            }
                            else -> {
                                "Maksimal skor Nilai adalah 100"
                            }
                        }
                }
            }
            4 -> {
                binding.form2.visibility = View.GONE
                binding.label.text = "Masukkan Huruf Konsonan/Vokal"
                binding.proses.setOnClickListener {
                    binding.hasil.text = when (binding.input.text.toString().toLowerCase(Locale.getDefault())) {
                        "a", "i", "u", "e", "o"  -> {
                            "Termasuk huruf vokal"
                        }
                        else -> {
                            "Bukan Termasuk huruf vokal"
                        }
                    }
                }
            }
            5 -> {
                binding.form2.visibility = View.GONE
                binding.label.text = "Masukkan tahun"
                binding.input.inputType = InputType.TYPE_CLASS_NUMBER
                binding.proses.setOnClickListener {
                    val input = binding.input.text.toString().toInt()
                    binding.hasil.text = when {
                        input % 400 == 0 -> {
                            " Merupakan Tahun Kabisat"
                        }
                        input % 100 == 0 -> {
                            " Bukan Tahun Kabisat"
                        }
                        input % 4 == 0 -> {
                            " Merupakan Tahun Kabisat"
                        }
                        else -> {
                            " Bukan Tahun Kabisat"
                        }
                    }
                }
            }
            6 -> {
                binding.form1.visibility = View.GONE
                binding.label2.text = "Penentuan Zodiak"
                binding.input1.inputType = InputType.TYPE_CLASS_NUMBER
                binding.input1.hint = "Masukkan tanggal"
                binding.input2.inputType = InputType.TYPE_CLASS_NUMBER
                binding.input2.hint = "Masukkan Bulang"
                binding.proses.setOnClickListener {
                    val tanggal = binding.input1.text.toString().toInt()
                    val bulan = binding.input2.text.toString().toInt()
                    binding.hasil.text = if ((tanggal>21 && bulan == 12) || (tanggal<=19 && bulan == 1)){
                        "Zodiak Anda Adalah Capricon"
                    }
                    else if ((tanggal>20 && bulan ==1) || (tanggal<=18 && bulan == 2)){
                        "Zodiak Anda adalah Aquarius"
                    }
                    else if ((tanggal>19 && bulan == 2) || (tanggal <=20 && bulan == 3)){
                        "Zodiak Anda Adalah Pisces"
                    }
                    else if ((tanggal>21 && bulan == 3) || (tanggal <=20 && bulan == 4)){
                        "Zodiak anda adalah aries"
                    }
                    else if ((tanggal>21 && bulan == 4) || (tanggal <=20 && bulan == 5)){
                        "Zodiak Anda Adalah Taurus"
                    }
                    else if ((tanggal>21 && bulan == 5) || (tanggal<=20 && bulan == 6)){
                        "Zodiak Anda Adalah Gemini"
                    }
                    else if ((tanggal>21 && bulan == 6) || (tanggal<=20 && bulan == 7)){
                        "Zodiak Anda Adalah Cancer"
                    }
                    else if ((tanggal>21 && bulan  == 7) || (tanggal<=20 && bulan== 8)){
                        "Zodiak Anda Adalah  Leo"
                    }
                    else if ((tanggal>21 && bulan ==8) || (tanggal<=22 && bulan == 9)){
                        "Zodiak Anda Adalah Virgo"
                    }
                    else if ((tanggal>23 && bulan == 9) || (tanggal<=20 && bulan ==10)){
                        "Zodiak Anda Adalah Libra"
                    }
                    else if ((tanggal>21 && bulan == 10) || (tanggal<=22 && bulan == 11)){
                        "Zodiak Anda Adalah Scorpio"
                    }
                    else if ((tanggal>23 && bulan == 11) || (tanggal<=20 && bulan == 12)){
                        "Zodiak Anda Adalah Sagitarius"
                    } else {
                        "Zodiak Anda Salah"
                    }
                }
            }
        }
    }
}