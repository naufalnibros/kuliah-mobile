package com.naufalnibros.kuliahmobile

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.naufalnibros.kuliahmobile.databinding.RecyclerviewItemMainBinding

class MainAdapter : RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    private val list: MutableList<String> = ArrayList()

    private var listener: OnItemListener? = null

    fun setList(list: List<String>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    fun setListener(listener: OnItemListener?) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                RecyclerviewItemMainBinding
                        .inflate(LayoutInflater
                                .from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.title.text = list[position]
        holder.itemView.setOnClickListener {
            listener?.onItemCLick(position)
        }

        holder.binding.container.setBackgroundColor(
                ResourcesCompat.getColor(holder.itemView.resources,
                        if (position % 2 == 0) R.color.purple_200 else R.color.purple_500, null))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(val binding: RecyclerviewItemMainBinding) : RecyclerView.ViewHolder(binding.root)

    interface OnItemListener {
        fun onItemCLick(position: Int)
    }
}